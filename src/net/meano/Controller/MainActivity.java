package net.meano.Controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Objects;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Context mContext;
	//	定位bool类型，是否按下连接键
	private boolean isConnected = false;
//	private TextView wifiStrength;
	//private LzSurfaceView controlSurfaceView;

	//	控制灯的按键定义
	private Button ledBtn;
	private Button linkBtnButton;
	
	private Button quickButton;
	private Button slowButton;
	private Button leftButton;
	private Button rightButton;
	private Button stopButton;
	private Button gSensorButton;
    private Button MidButton;

	private SeekBar LServoBar;
    private SeekBar RServoBar;

    private TextView LServoText;
    private TextView RServoText;
	//	TCP/IP进程
	private Thread mThreadClient = null;
	private Socket mSocketClient = null;
	//	数据流
	static InputStream mBufferedReaderClient = null;
	static OutputStream mPrintWriterClient = null;
	
	private String recMsgString = " ";
	
//	private TextView recvTextView;
    private byte START_SIG = (byte) 0XAA;
    private byte END_SIG = (byte) 0XFC;
    private byte[] SPEED_COUNT = {(byte) 0xd0,(byte) 0xd1,(byte) 0xd2,(byte) 0xd3,
    		(byte) 0xd4,(byte) 0xd5,(byte) 0xd6,(byte) 0xd7,(byte) 0xd8,(byte) 0xd9,
    		(byte) 0xda,(byte) 0xdb,(byte) 0xdc,(byte) 0xdd,(byte) 0xde,(byte) 0xdf};
    private byte[] DIRECT_COUNT = {(byte) 0xee,(byte) 0xed,(byte) 0xec,(byte) 0xeb,(byte) 0xea,
    		(byte) 0xe9,(byte) 0xe8,(byte) 0xe7,(byte) 0xe6,(byte) 0xe5,(byte) 0xe4,(byte) 0xe3,(byte) 0xe2,
    		(byte) 0xe1,(byte) 0xe0,(byte) 0xe0};
    private byte[] FISH_ID = { (byte) 0x91,(byte) 0x92,(byte) 0x93,(byte) 0x94,(byte) 0x95,(byte) 0x96,
    		(byte) 0x97,(byte) 0x98,(byte) 0x99,(byte) 0x9a,(byte) 0x9b,(byte) 0x9c,(byte) 0x9d,
    		(byte) 0x9e,(byte) 0x9f,(byte) 0x90};

    private WifiManager wmManager;
    private WifiInfo wifiInfo;
    private int strength;
	//1、取得重力感应器Sensor对象
  	//在 Activity 中定义以下成员变量：
	private boolean isGSensor = false;
//    private TextView gSensorTextView;
   	private Thread gSensorThread = null;
	private SensorManager mManager = null;
	private Sensor mSensor = null;
	private SensorEventListener gSensorEventListener;
  	private float x=0,y=0,z=0;
	private String gSensorMsgString = " ";

	//重连次数
	private int reconnectCount = 0;
	private int linkBtnState = 0;
	private boolean isFishStop = true;
	private int speedMode = 0;

	private static String TAG = "yu controller";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		mContext = this;
		//通过服务得到传感器管理对象
		mManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensor = mManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		gSensorEventListener = new SensorEventListener() {
			@Override
			public void onSensorChanged(SensorEvent event) {
				// TODO Auto-generated method stub
				x=event.values[0];
				y=event.values[1];
				z=event.values[2];
			}
			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {
				// TODO Auto-generated method stub
			}
		};
		mManager.registerListener(gSensorEventListener, mSensor, SensorManager.SENSOR_DELAY_GAME);

		
//		recvTextView = (TextView)findViewById(R.id.recText);
//		recvTextView.setMovementMethod(ScrollingMovementMethod.getInstance());
		
		ledBtn = (Button)findViewById(R.id.controlLedBtn);
		ledBtn.setOnClickListener(LedBtnLisnter);
		
		linkBtnButton = (Button)findViewById(R.id.linkBtn);
		linkBtnButton.setOnClickListener(LinkClickListener);
		
		quickButton = (Button)findViewById(R.id.quickBtn);
		quickButton.setOnClickListener(QuickOnClick);
		slowButton = (Button)findViewById(R.id.slowBtn);
		slowButton.setOnClickListener(SlowOnClick);
		leftButton = (Button)findViewById(R.id.leftBtn);
		leftButton.setOnClickListener(LeftOnClick);
		rightButton = (Button)findViewById(R.id.rightBtn);
		rightButton.setOnClickListener(RightOnClick);
		stopButton = (Button)findViewById(R.id.stopBtn);
		stopButton.setOnClickListener(StopOnClick);
		MidButton = (Button)findViewById(R.id.MidBtn);
        MidButton.setOnClickListener(MidButtonOnClick);
		gSensorButton = (Button)findViewById(R.id.gSensorBtn);
		gSensorButton.setOnClickListener(GSensorOnClick);
        LServoBar = (SeekBar)findViewById(R.id.LeftServoSeekBar);
        LServoBar.setOnSeekBarChangeListener(OnLServoChange);
        RServoBar = (SeekBar)findViewById(R.id.RightServoSeekBar);
        RServoBar.setOnSeekBarChangeListener(OnRServoChange);

        LServoText = (TextView)findViewById(R.id.LeftText);
        RServoText = (TextView)findViewById(R.id.RightText);
        LServoBar.setEnabled(false);
        RServoBar.setEnabled(false);
//		wifiStrength = (TextView)findViewById(R.id.wifiStrength);
	}

    private SeekBar.OnSeekBarChangeListener OnLServoChange = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            LServoText.setText("左:" + progress);
            byte[] mySend = { START_SIG, FISH_ID[0], (byte) ((0xA0+progress)&0xFF), END_SIG};
            sendMessage(mySend);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            LServoText.setText("左:" + seekBar.getProgress());
            byte[] mySend = { START_SIG, FISH_ID[0], (byte) ((0xA0+seekBar.getProgress())&0xFF), END_SIG };
            sendMessage(mySend);
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            LServoText.setText("左:" + seekBar.getProgress());
            byte[] mySend = { START_SIG, FISH_ID[0], (byte) ((0xA0+seekBar.getProgress())&0xFF), END_SIG };
            sendMessage(mySend);
        }
    };

    private SeekBar.OnSeekBarChangeListener OnRServoChange = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            RServoText.setText("右:" + progress);
            byte[] mySend = { START_SIG, FISH_ID[0], (byte) ((0xB0+progress)&0xFF), END_SIG };
            sendMessage(mySend);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            RServoText.setText("右:" + seekBar.getProgress());
            byte[] mySend = { START_SIG, FISH_ID[0], (byte) ((0xB0+seekBar.getProgress())&0xFF), END_SIG };
            sendMessage(mySend);
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            RServoText.setText("右:" + seekBar.getProgress());
            byte[] mySend = { START_SIG, FISH_ID[0], (byte) ((0xB0+seekBar.getProgress())&0xFF), END_SIG};
            sendMessage(mySend);
        }
    };

	private OnClickListener LedBtnLisnter =new OnClickListener() {
		public void onClick(View v) {
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, fishSet.class);
			
			Bundle bundle = new Bundle();
			bundle.putString("netStata", linkBtnButton.getText().toString());
			intent.putExtras(bundle);
			startActivityForResult(intent, 0);
		}
	};

	private OnClickListener StopOnClick = new OnClickListener() {
		public void onClick(View v) {
			DirectControl((byte) 7);
			try {
				Thread.sleep(50);
			} catch (Exception e) {

			}
			SpeedControl((byte)0);

			//停止时取消重力感应
			if(isGSensor) {
				gSensorButton.performClick();
			}
			//禁用一些按钮
			isFishStop = true;
			speedMode = 0;
			onFishStop();
		}
	};

	private OnClickListener RightOnClick = new OnClickListener() {
		public void onClick(View v) {
			DirectControl((byte) 14);
		}
	};

	private OnClickListener LeftOnClick = new OnClickListener() {
		public void onClick(View v) {
			DirectControl((byte) 1);
		}
	};

	private OnClickListener MidButtonOnClick = new OnClickListener() {
        public void onClick(View v) {
            DirectControl((byte) 7);
        }
    };

	private OnClickListener SlowOnClick = new OnClickListener() {
		public void onClick(View v) {
			SpeedControl((byte)8);
			isFishStop = false;
			speedMode = 1;
			onFishStop();
		}
	};

	private OnClickListener QuickOnClick = new OnClickListener() {
		public void onClick(View v) {
			SpeedControl((byte)14);
			isFishStop = false;
			speedMode = 2;
			onFishStop();
		}
	};

	private OnClickListener LinkClickListener = new OnClickListener() {
		public void onClick(View v) {
			linkBtnButton.setEnabled(false);
			if(isConnected){
				//已连接，再次点击时关闭
				linkBtnState = 0;
				DirectControl((byte)7);
				try {
					Thread.sleep(50);
				} catch (Exception e) {

				}
				SpeedControl((byte)0);
				doClose();
			}
			else {
				//未连接，再次点击连接
				linkBtnState = 1;
				doConnect();
			}
		}
	};

	private void doConnect(){
		mThreadClient = new Thread(linkRunnable);
		mThreadClient.start();
	}

	private void doClose(){
		DirectControl((byte)7);
		SpeedControl((byte) 0);

		//主断点关闭，不再重连
		onDisconnect(null, false);

		if(mThreadClient != null) {
			mThreadClient.interrupt();
			mThreadClient = null;
		}
	}

	private Runnable linkRunnable = new Runnable() {
		public void run() {
			String msgTextString = "192.168.0.1";
//			String msgTextString = "192.168.0.100";
			int port = 8899;

			try {
				Log.i(TAG, "try to connect to server");
				//连接
				mSocketClient = new Socket();
				//5秒连接超时
				mSocketClient.connect(new InetSocketAddress(msgTextString, port), 5000);
				//无论数据包的大小，实时发送
				mSocketClient.setTcpNoDelay(true);
				mBufferedReaderClient = mSocketClient.getInputStream();
				mPrintWriterClient = mSocketClient.getOutputStream();
				isConnected = true;
			}
			catch(Exception e){
				e.printStackTrace();
				isConnected = false;
			}
			if(isConnected) {
				//连接成功
				Log.i(TAG, "connect success");
				recMsgString = "已经连接Server!\n";
				Message msgMessage = new Message();
				msgMessage.what = 2;
				ControllerHandler.sendMessage(msgMessage);

				receiveMessage();
			}else{
				//连接失败，尝试重连
				Log.i(TAG, "connect fail");
				onDisconnect("连接失败", true);
			}
		}
	};

	private OnClickListener GSensorOnClick = new OnClickListener() {
		public void onClick(View v) {
			if (isGSensor) {
				isGSensor = false;
				gSensorThread.interrupt();
				gSensorThread = null;
				gSensorButton.setText("使用重力感应");
			} else {
				isGSensor = true;
				gSensorThread = new Thread(gSensorRunnable);
				gSensorThread.start();
				gSensorButton.setText("取消重力感应");
			}
		}
	};

	private Runnable gSensorRunnable = new Runnable() {
		public void run() {
			while (isGSensor) {
				//gSensorMsgString = "X:" + Float.toString(x) + "Y:" + Float.toString(y) + "Z:" + Float.toString(z);
				gSensorMsgString = String.format("X: %.3f Y: %.3f Z: %.3f", x, y, z);
				if (x < -7) {
					x = -7;
				}
				if (x > 7) {
					x = 7;
				}
				DirectControl((byte) (7 - x));

				if (y < -6) {
					y = -6;
				}
				if (y > 4) {
					SpeedControl((byte) 0);
				} else {
					SpeedControl((byte) (9 - y));
				}

				Message msgMessage = new Message();
				msgMessage.what = 0;
				ControllerHandler.sendMessage(msgMessage);
				try {
					Thread.sleep(500);
				} catch (Exception e) {

				}
			}
		}
	};

	Handler ControllerHandler = new Handler(){
		public void handleMessage(Message msgMessage) {
			super.handleMessage(msgMessage);
			if (msgMessage.what == 0) {
//				gSensorTextView.setText("重力感应值："+gSensorMsgString);
				wmManager = (WifiManager)getSystemService(WIFI_SERVICE);
				wifiInfo = wmManager.getConnectionInfo();
				if(wifiInfo.getSSID()!=null){
					strength = wifiInfo.getRssi();
				}
//				wifiStrength.setText("网络信号强度：" + Integer.toString(strength));
			} else if(msgMessage.what == 1) {
//				recvTextView.append("client:"+recMsgString);
				Log.i(TAG, recMsgString);
			}else if(msgMessage.what == 2){
				//连接成功
//				recvTextView.append("client:"+ recMsgString);
				Log.i(TAG, recMsgString);
				setButtonEnabled(true);
				speedMode = 0;
				onFishStop();
				linkBtnButton.setEnabled(true);
        		linkBtnButton.setText("关闭");
				reconnectCount = 0;
				Toast.makeText(mContext, "连接成功", Toast.LENGTH_SHORT).show();
			}else if(msgMessage.what == 3){
				//连接断开
				isConnected = false;
				isFishStop = true;

				if(isGSensor){
					isGSensor = false;
					gSensorButton.setText("使用重力感应");
				}

				if(gSensorThread != null){
					gSensorThread.interrupt();
					gSensorThread = null;
				}

				try {
					if(mSocketClient != null) {
						mSocketClient.close();
						mSocketClient = null;
					}
					if(mBufferedReaderClient != null) {
						mBufferedReaderClient.close();
						mBufferedReaderClient = null;
					}
					if(mPrintWriterClient != null) {
						mPrintWriterClient.close();
						mPrintWriterClient = null;
					}
				}
				catch (IOException ex){
					ex.printStackTrace();
				}

				//其它按钮不可用
				setButtonEnabled(false);
				linkBtnButton.setText("连接");

				boolean needReconnect = msgMessage.getData().getBoolean("needReconnect");
				String msg = msgMessage.getData().getString("message");
				if(linkBtnState == 1 && needReconnect){
					//如果需要重连
					if(reconnectCount < 3){
						//重连次数不超过3次
						reconnectCount++;
						Toast.makeText(mContext, String.format("连接失败，尝试第%d次重连", reconnectCount), Toast.LENGTH_SHORT).show();

						//连接服务器
						linkBtnButton.performClick();
					}else{
						reconnectCount = 0;
						linkBtnButton.setEnabled(true);
						Toast.makeText(mContext, "连接不上服务器", Toast.LENGTH_SHORT).show();
					}
				}else {
					linkBtnButton.setEnabled(true);
					if(msg != null && msg != "") {
						Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
					}
				}
			}
		}
	};

	private void DirectControl(byte direct){
		byte[] mySend = { START_SIG, FISH_ID[0], DIRECT_COUNT[direct], END_SIG };
		sendMessage(mySend);
	}

	private void SpeedControl(byte speed){
		byte[] mySend = { START_SIG, FISH_ID[0], SPEED_COUNT[speed], END_SIG };
		sendMessage(mySend);
	}

	private void sendMessage(byte []msg) {
		if (isConnected) {
			try {
				Log.i(TAG, "send " + msg);
				mPrintWriterClient.write(msg);
				mPrintWriterClient.flush();
			} catch (Exception e) {
				Log.i(TAG, "send error");
				isConnected = false;
				Toast.makeText(mContext, "发送异常" + e.getMessage(), Toast.LENGTH_SHORT).show();
				onDisconnect("连接断开", true);
			}
		} else {
			Toast.makeText(mContext, "还未连接平台", Toast.LENGTH_SHORT).show();
		}
	}

	private void receiveMessage(){
		byte[] buffer = new byte[256];
		while(isConnected) {
			try {
				if (mBufferedReaderClient.read(buffer) > 0) {
					Log.i(TAG, "receive " + buffer);
					recMsgString = Integer.toHexString(buffer[0] & 0xff) + "\n";
					Message msgMessage = new Message();
					msgMessage.what = 1;
					ControllerHandler.sendMessage(msgMessage);
				}
			} catch (Exception e) {
				Log.i(TAG, "receive error");
				isConnected = false;
				recMsgString = "接收异常" + e.getMessage() + "\n";
				onDisconnect("连接断开", true);
			}
		}
	}

	private void onFishStop(){
		leftButton.setEnabled(!isFishStop);
		rightButton.setEnabled(!isFishStop);
		MidButton.setEnabled(!isFishStop);
		stopButton.setEnabled(!isFishStop);
		slowButton.setEnabled(speedMode != 1);
		quickButton.setEnabled(speedMode != 2);
	}

	private void setButtonEnabled(boolean value){
		ledBtn.setEnabled(value);
		gSensorButton.setEnabled(value);

		quickButton.setEnabled(value);
		slowButton.setEnabled(value);

		leftButton.setEnabled(value);
		rightButton.setEnabled(value);
		stopButton.setEnabled(value);
		MidButton.setEnabled(value);

        LServoBar.setEnabled(value);
        RServoBar.setEnabled(value);
	}

    //needReconnect: 是否需要重连
    private void onDisconnect(String msg, boolean needReconnect){
		Message msgMessage = new Message();
		msgMessage.what = 3;
		Bundle bundle = new Bundle();
		bundle.putBoolean("needReconnect", needReconnect);
		bundle.putString("message", msg);
		msgMessage.setData(bundle);

		ControllerHandler.sendMessage(msgMessage);
    }

    @Override
	protected void onDestroy() {
        super.onDestroy();
        
        //不再重连
        onDisconnect(null, false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
