package net.meano.Controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class fishSet extends Activity {
	private Button backButton;
	private TextView secondTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.fish_setting);
		secondTextView = (TextView)findViewById(R.id.textView1);
		Bundle bundle = this.getIntent().getExtras();
		String stataString = bundle.getString("netStata");
		secondTextView.append("现在连接按键的状态是："+stataString);
		backButton = (Button)findViewById(R.id.backForward);
		backButton.setOnClickListener(backOnClick);
	}
	private OnClickListener backOnClick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent= new Intent();
			intent.setClass(fishSet.this, MainActivity.class);
			fishSet.this.setResult(RESULT_OK);//startActivity(intent);
			fishSet.this.finish();
		}
	};

}
